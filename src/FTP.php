<?php
/**
 * The core class for the ftp connection
 *
 * @category    FTP
 * @author      Frank Giger <frank.giger@behires.com>
 * @version     1.0
 */
namespace Behires\FTP;

class FTP
{
    /**
     * The connection to the FTP-Server
     *
     * @var resource
     */
    private $connection;

    /**
     * FTP constructor.
     *
     * @param string $host
     * @param bool   $useTLS
     * @param int    $port
     * @param int    $timeout
     *
     * @throws \Exception
     */
    public function __construct($host, $useTLS = true, $port = 21, $timeout = 30)
    {
        // Init a connection to the server  :
        $connection = ($useTLS ? ftp_ssl_connect($host, $port, $timeout) : ftp_connect($host, $port, $timeout));

        if(!$connection) {
            throw new \Exception("Can't connect to the content server.");
        }

        $this->connection = $connection;
    }

    /**
     * When the script is at the end it will close the connection to the ftp server
     */
    public function __destruct()
    {
        $this->closeConnection();
    }

    /**
     * Login to the server
     *
     * @param string $username
     * @param string $password
     *
     * @return bool
     */
    public function login($username, $password)
    {
        if(!@ftp_login($this->connection, $username, $password)) {
            return false;
        }
        return true;
    }

    /**
     * Get the current directory name
     *
     * @return string
     */
    public function getCurrentDirectory()
    {
        return ftp_pwd($this->connection);
    }

    /**
     * Go one directory up
     *
     * @return bool
     */
    public function changeToParentDirectory()
    {
        return ftp_cdup($this->connection);
    }

    /**
     * Go into this directory
     *
     * @param string $directory
     *
     * @return bool
     */
    public function changeDirectoryTo($directory)
    {
        return ftp_chdir($this->connection, $directory);
    }

    /**
     * Change the permission from the file
     *
     * @param int    $mode
     * @param string $file
     *
     * @return int|bool
     */
    public function changePermissionOfFile($mode = 0644, $file)
    {
        return ftp_chmod($this->connection, $mode, $file);
    }

    /**
     * Close the connection
     *
     * @return bool
     */
    public function closeConnection()
    {
        return ftp_close($this->connection);
    }

    /**
     * Delete a file on the server
     *
     * @param string $file
     *
     * @return bool
     */
    public function deleteFile($file)
    {
        return ftp_delete($this->connection, $file);
    }

    /**
     * Get the last modification from the file
     *
     * @param string $file
     *
     * @return int
     */
    public function lastModificationFromFile($file)
    {
        return ftp_mdtm($this->connection, $file);
    }

    /**
     * Create a new directory on the server
     *
     * @param string $dir
     *
     * @return string
     */
    public function createDirectory($dir)
    {
        return ftp_mkdir($this->connection, $dir);
    }

    /**
     * Get a list from the directory with the files
     *
     * @param $dir
     *
     * @return array
     */
    public function getListFromDirectory($dir)
    {
        return ftp_nlist($this->connection, $dir);
    }

    /**
     * Enable the passive mode
     */
    public function enablePassiveMode()
    {
        ftp_pasv($this->connection, true);
    }

    /**
     * Disable the passive mode
     */
    public function disablePassiveMode()
    {
        ftp_pasv($this->connection, false);
    }

    /**
     * Upload a file from the local storage
     *
     * @param string $localPath
     * @param string $remotePath
     * @param int    $mode
     *
     * @return bool
     */
    public function uploadFile($localPath, $remotePath, $mode = FTP_BINARY)
    {
        return ftp_put($this->connection, $remotePath, $localPath, $mode);
    }

    /**
     * Upload a file from a string
     *
     * @param string $data
     * @param string $remotePath
     * @param int    $mode
     *
     * @return bool
     */
    public function uploadFileFromString($data, $remotePath, $mode = FTP_BINARY)
    {
        // Create a temporary file
        $file = tempnam(sys_get_temp_dir(), "FTP");
        file_put_contents($file, $data);

        $handle = fopen($file, "r");
        $result = $this->uploadFileFromResource($handle, $remotePath, $mode);

        // Delete the temporary file
        fclose($handle);
        unlink($file);

        return $result;
    }

    /**
     * Upload a file from a resource (fopen)
     *
     * @param Resource $handle
     * @param string   $remotePath
     * @param int      $mode
     *
     * @return bool
     */
    public function uploadFileFromResource($handle, $remotePath, $mode = FTP_BINARY)
    {
        return ftp_fput($this->connection, $remotePath, $handle, $mode);
    }

    /**
     * Rename a file
     *
     * @param string $old
     * @param string $new
     *
     * @return bool
     */
    public function renameFile($old, $new)
    {
        return ftp_rename($this->connection, $old, $new);
    }

    /**
     * Remove a directory
     *
     * @param string $dir
     *
     * @return bool
     */
    public function removeDirectory($dir)
    {
        return ftp_rmdir($this->connection, $dir);
    }

    /**
     * Get the size of the file
     *
     * @param string $file
     *
     * @return int
     */
    public function getFileSize($file)
    {
        return ftp_size($this->connection, $file);
    }
}